"use strict";

// 1. Запитайте у користувача два числа.
// Перевірте, чи є кожне з введених значень числом.
// Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом.
// Виведіть на екран всі цілі числа від меншого до більшого за допомогою циклу for.

let firstNumber = prompt(`Введіть перше число`);
if (firstNumber !== null && firstNumber !== "" && !isNaN(firstNumber)) {
  console.log(`Введено число. Це число ${firstNumber}`);
} else {
  alert(`Введено не число! Будьласка, спробуйте ще раз`);
  while (firstNumber) {
    let firstNumber = prompt(`Введіть перше число`);
    if (firstNumber !== null && firstNumber !== "" && !isNaN(firstNumber)) {
      console.log(`Введено число. Це число ${firstNumber}`);
      break;
    }
  }
}
let secondNumber = prompt(`Введіть друге число`);
if (secondNumber !== null && secondNumber !== "" && !isNaN(secondNumber)) {
  console.log(`Введено число. Це число ${secondNumber}`);
} else {
  alert(`Введено не число! Будьласка, спробуйте ще раз`);
  while (secondNumber) {
    let secondNumber = prompt(`Введіть друге число`);
    if (secondNumber !== null && secondNumber !== "" && !isNaN(secondNumber)) {
      console.log(`Введено число. Це число ${secondNumber}`);
      break;
    }
  }
}
if (firstNumber > secondNumber) {
  for (let i = secondNumber; i <= firstNumber; i++) {
    console.log(i);
  }
} else if (secondNumber > firstNumber) {
  for (let i = firstNumber; i <= secondNumber; i++) {
    console.log(i);
  }
}

// 2. Напишіть програму, яка запитує в користувача число та перевіряє, чи воно є парним числом. Якщо введене значення не є парним числом, то запитуйте число доки користувач не введе правильне значення.

let userNumber = prompt(`Введіть число`);
if (userNumber !== null && userNumber !== "" && !isNaN(userNumber)) {
  if (userNumber % 2 === 0) {
    console.log(`Введене число є парним`);
  } else {
    alert(`Введене число не є парним. Спробуйте ще раз`);
    while (userNumber) {
      let userNumber = prompt(`Введіть число`);
      if (userNumber !== null && userNumber !== "" && !isNaN(userNumber)) {
        if (userNumber % 2 === 0) {
          console.log(`Введене число є парним`);
          break;
        }
      }
    }
  }
}
